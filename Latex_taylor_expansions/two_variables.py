# Prints LaTeX of thr expantion of a function u(x + nk, t + mh) in terms of u(x, t)
from fractions import Fraction
from math import factorial
import numpy as np

order = 5  # Order to expand to

function = "u"  # u

v1 = "x"  # x
v2 = "t"  # t

delta_v1 = "k"  # k
delta_v2 = "h"  # h

delta_v1_multi = 1 / 2  # n
delta_v2_multi = 1  # m


def frac_str(f: Fraction) -> str:
    return "\\frac\{" + f"{f.numerator}" + "}{" + f"{f.denominator}" + "}"


def gen_dv_str(dv, dvm):
    if dvm == 0:
        return ""
    dv_str = ""
    if dvm < 0:
        dv_str += " - "
        dvm = -dvm
    else:
        dv_str += " + "
    if dvm != 1:
        if int(dvm) != dvm:
            dv_str += frac_str(Fraction(dvm))
        else:
            dv_str += f"{dvm}"
    dv_str += dv
    return dv_str


exp = f"{function}({v1}{gen_dv_str(delta_v1, delta_v1_multi)}, {v2}{gen_dv_str(delta_v2, delta_v2_multi)}) = "

for i in range(order + 1):
    for k in [0, i] + list(range(1, i)):
        j = i - k
        f = Fraction((delta_v1_multi ** j) * (delta_v2_multi ** k)) / Fraction(
            factorial(j) * factorial(k)
        )
        num = f.numerator
        den = f.denominator
        if num != 0:
            if num < 0:
                exp += " - "
                num = -num
            else:
                exp += " + "
            if num > 1:
                num = f"{int(num)}"
            else:
                num = ""
            if j > 0:
                if j > 1:
                    num += f"{delta_v1}^{j}"
                else:
                    num += delta_v1
            if j > 0 and k > 0:
                num += " "
            if k > 0:
                if k > 1:
                    num += f"{delta_v2}^{k}"
                else:
                    num += delta_v2
            if den > 1:
                if len(num) == 0:
                    num = "1"
                exp += "\\frac{" + f"{num}" + "}{" + f"{den}" + "} "
            else:
                exp += f"{num} "
            index = ""
            for p in range(j):
                index += v1
            for p in range(k):
                index += v2
            exp += function
            if len(index) > 0:
                exp += "_{" + f"{index}" + "}"

exp += " + ..."

print(exp)
